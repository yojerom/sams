<?php

use Illuminate\Support\Facades\Route;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// auth route for both
Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name ('dashboard');
});

//for users
Route::group(['middleware' => ['auth', 'role:student']], function () {
    Route::get('/dashboard.myprofile', 'App\Http\Controllers\DashboardController@myprofile')->name ('dashboard.myprofile');
});

//for Teachers
Route::group(['middleware' => ['auth', 'role:teacher']], function () {
    Route::get('/dashboard.genqr', 'App\Http\Controllers\DashboardController@genqr')->name ('dashboard.genqr');
});

require __DIR__.'/auth.php';

/* // for mail verification - user role based
Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index', function (){
    return view('dashboard');
})-> middleware(['auth', 'verified'])->name('dashboard');


 Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');


Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/dashboard');
})->middleware(['auth', 'signed'])->name('verification.verify');


Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send'); */
