<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        if (Auth::user()->hasRole('student'))
        {
            return view('studentdash');
 
        }
        elseif(Auth::user()->hasRole('teacher'))
        {
            return view('teacherdash');
 
        }
        elseif(Auth::user()->hasRole('admin'))
        {
            return view('dashboard');
 
        }
    }

    public function myprofile()
    {
        return view('myprofile');
    }
    
    public function genqr()
    {
        return view('genqr');
    }
}
