<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lecturer_attendance extends Model
{
    use HasFactory;
    
    protected $table="lecturer_attendances";

    public function lecturer()
    {
        return $this->belongsTo(Lecturer::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

}
