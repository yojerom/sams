<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    use HasFactory;

    protected $table="lecturers";

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function subject()
    {
        return $this->hasMany('Subject::class');
    }
   
    public function lecturerAttendance()
    {
        return $this->hasMany('Lecturer_attendance::class');
    }
}
