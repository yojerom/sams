<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faculties extends Model
{
    use HasFactory;

    protected $table="faculties";

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function department()
    {
        return $this->hasMany(Department::class);
    }
}
