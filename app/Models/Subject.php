<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    protected $table="subjects";

    public function lecturer()
    {
        return $this->belongsTo(Lecturer::class);
    }

    public function batch()
    {
        return $this->belongsTo(Batch::class);
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }
    
    public function module()
    {
        return $this->belongsTo('App\Models\Module');
    }

    public function classes()
    {
        return $this->hasMany('Classes::class');
    }

    public function lecturerAttendance()
    {
        return $this->hasMany('Lecturer_attendance::class');
    }
}
