<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;
   
    protected $table="classes";

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function studentAttendance()
    {
        return $this->hasMany('Student_attendance::class');
    }
}
