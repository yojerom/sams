<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    use HasFactory;
    
    protected $table="semesters";

    public function student()
    {
        return $this->hasMany('Student::class');
    }

    public function subject()
    {
        return $this->hasMany('Subject::class');
    }
}
