<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Register</title>

    @include('includes.stylesheets')

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">AMS</h1>

            </div>
            <h3>Register to SAMS</h3>
            
            <form class="m-t" method="POST" action="{{route('register')}}">
            @csrf
                <div class="form-group">
                    <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control" placeholder="Name" required="">
                </div>

                <div class="form-group">
                    <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email" required="">
                </div>
                <div class="form-group">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <div class="form-group">
                    <input type="password" id="password-confirm" name="password_confirmation"  class="form-control" placeholder="Confirm Password" required="">
                </div>
                
                <div class="form-group">
                     <x-label for="role_id" value="{{ _('Register as:') }}" />               
                        <select name="role_id" class="form-group">
                        <option value="admin">Admin</option>
                       <option value="teacher">Teacher</option>
                       <option value="student">Student</option>                                                       
                        </select>
                        </div>

               <!--  <div class="form-group">
                        <div class="checkbox i-checks"><label> <input type="checkbox"><i></i> Agree the terms and policy </label></div>
                </div> -->
                <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

                <p class="text-muted text-center"><small>Already have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{route('login')}}">Login</a>
            </form>
           
        </div>
    </div>

    <!-- Mainly scripts -->
    @include('includes.javascripts')
</body>

</html>
