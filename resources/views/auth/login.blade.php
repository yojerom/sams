<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> Login</title>

    @include('includes.stylesheets')

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">AMS</h1>

            </div>
           
           
      
            <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
            @csrf
                <div class="form-group">
                    <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Username" required="">
                </div>

                
                <div class="form-group">
                    <input type="password" id="password" class="form-control" name="password" placeholder="Password" required="">
                </div>

                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href="#"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{route('register')}}">Create an account</a>
            </form>
          
        </div>
    </div>

    <!-- Mainly scripts -->
    @include('includes.javascripts')

</body>

</html>
